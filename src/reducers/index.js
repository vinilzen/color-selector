import { combineReducers } from 'redux';
import _ from 'lodash';
import {
  SELECT_COLOR,
  SEARCH_COLOR,
  PICK_COLOR,
  REQUEST_COLORS,
  RECIEVE_COLORS,
  FETCH_COLORS_FAILURE
} from '../actions';


const selectedColor = (state = '', action) => {
  switch (action.type) {
  case SELECT_COLOR:
    return action.color;
  default:
    return state;
  }
};


const fetchFailure = (state = '', action) => {
  switch (action.type) {
  case FETCH_COLORS_FAILURE:
    return action.ex;
  default:
    return state;
  }
};

const searchString = (state = '', action) => {
  switch (action.type) {
  case SEARCH_COLOR:
    return action.string;
  default:
    return state;
  }
};

const backgroundColor = (state = 'ffffff', action) => {
  switch (action.type) {
  case PICK_COLOR:
    return `${action.backgroundColor.hex}7f`; // 0.498% transparency
  default:
    return state;
  }
};

const fetching = (state = {
  isFetching: true,
  colors: []
}, action) => {
  switch (action.type) {
  case RECIEVE_COLORS:
    return {
      ...state,
      isFetching: false,
      colors: _.uniqBy(action.colors, color => color.hex)
    };
  default:
    return state;
  }
};

const colorsByMocky = (state = {
  isFetching: false,
  colors: []
}, action) => {
  switch (action.type) {
  case RECIEVE_COLORS:
    return {
      ...state,
      colors: fetching(state[action], action).colors,
      isFetching: false
    };
  case REQUEST_COLORS:
    return {
      ...state,
      isFetching: true
    };
  case FETCH_COLORS_FAILURE:
    return {
      ...state,
      isFetching: false
    };
  default:
    return state;
  }
};


const rootReducer = combineReducers({
  fetchFailure,
  colorsByMocky,
  selectedColor,
  searchString,
  backgroundColor
});

export default rootReducer;
