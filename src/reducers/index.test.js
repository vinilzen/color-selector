import reducer from './index';
import { SELECT_COLOR, PICK_COLOR, REQUEST_COLORS } from '../actions';

describe('colors reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      backgroundColor: 'ffffff',
      colorsByMocky: {
        colors: [],
        isFetching: false
      },
      fetchFailure: '',
      searchString: '',
      selectedColor: ''
    });
  });


  it('should handle SELECT_COLOR', () => {
    expect(reducer({}, {
      type: SELECT_COLOR,
      color: 'f0f0f0'
    })).toEqual({
      selectedColor: 'f0f0f0',
      backgroundColor: 'ffffff',
      colorsByMocky: {
        colors: [],
        isFetching: false
      },
      fetchFailure: '',
      searchString: ''
    });
  });


  it('should handle REQUEST_COLORS', () => {
    expect(reducer({}, {
      type: REQUEST_COLORS,
      isFetching: true
    })).toEqual({
      backgroundColor: 'ffffff',
      selectedColor: '',
      colorsByMocky: {
        colors: [],
        isFetching: true
      },
      fetchFailure: '',
      searchString: ''
    });
  });

  it('should handle PICK_COLOR', () => {
    expect(reducer({}, {
      type: PICK_COLOR,
      backgroundColor: {
        name: 'white',
        hex: '000000'
      }
    })).toEqual({
      backgroundColor: '0000007f',
      selectedColor: '',
      colorsByMocky: {
        colors: [],
        isFetching: false
      },
      fetchFailure: '',
      searchString: ''
    });
  });
});
