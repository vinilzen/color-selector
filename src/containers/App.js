import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { filterColor, fetchColorsIfNeeded, pickColor } from '../actions';
import ListOfColors from '../components/ListOfColors';
import SearchForm from '../components/SearchForm';

class App extends Component {
  static propTypes = {
    selectedColor: PropTypes.string.isRequired,
    searchString: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    colors: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      hex: PropTypes.string.isRequired
    })).isRequired,
    isFetching: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  componentDidMount() {
    const { dispatch, selectedColor } = this.props;
    dispatch(fetchColorsIfNeeded(selectedColor));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedColor !== this.props.selectedColor) {
      const { dispatch, selectedColor } = nextProps;
      dispatch(fetchColorsIfNeeded(selectedColor));
    }
  }

  handleChange = (nextColor) => {
    this.props.dispatch(filterColor(nextColor));
  }

  handleChoose = (nextColor) => {
    this.props.dispatch(pickColor(nextColor));
  }

  render() {
    const {
      searchString,
      colors,
      backgroundColor,
      isFetching,
      message
    } = this.props;

    return (
      <div
        className="card"
        id="app"
        style={{
          background: `#${backgroundColor}`
        }}
      >
        <div className="card-body">
          <SearchForm onChange={this.handleChange} />
          <ListOfColors
            colors={colors}
            searchString={searchString}
            pickColor={this.handleChoose}
            isFetching={isFetching}
            message={message}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    fetchFailure,
    selectedColor,
    searchString,
    colorsByMocky,
    backgroundColor
  } = state;

  const {
    isFetching,
    colors
  } = colorsByMocky || {
    isFetching: true,
    colors: []
  };

  const {
    message
  } = fetchFailure || {
    message: ''
  };

  return {
    selectedColor,
    searchString,
    colors,
    isFetching,
    backgroundColor,
    message
  };
};

export default connect(mapStateToProps)(App);
