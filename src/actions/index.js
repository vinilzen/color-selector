export const REQUEST_COLORS = 'REQUEST_COLORS';
export const RECIEVE_COLORS = 'RECIEVE_COLORS';
export const SELECT_COLOR = 'SELECT_COLOR';
export const SEARCH_COLOR = 'SEARCH_COLOR';
export const PICK_COLOR = 'PICK_COLOR';
export const FETCH_COLORS_FAILURE = 'FETCH_COLORS_FAILURE';


export const selectColor = color => ({
  type: SELECT_COLOR,
  color
});

export const filterColor = string => ({
  type: SEARCH_COLOR,
  string
});

export const pickColor = backgroundColor => ({
  type: PICK_COLOR,
  backgroundColor
});

export const requestColors = () => ({
  type: REQUEST_COLORS,
  isFetching: true
});

export const receiveColors = json => ({
  type: RECIEVE_COLORS,
  colors: json
});

export const fetchColorsFailure = ex => ({
  type: FETCH_COLORS_FAILURE,
  ex
});

export const fetchColors = () => (dispatch) => {
  dispatch(requestColors());
  return fetch('http://www.mocky.io/v2/5a37a7403200000f10eb6a2d')
    .then(response => response.json())
    .then(json => dispatch(receiveColors(json)))
    .catch(ex => dispatch(fetchColorsFailure(ex)));
};

const shouldFetchColors = (state) => {
  if (!state.colorsByMocky) {
    return true;
  }
  if (state.colorsByMocky.isFetching) {
    return false;
  }
  return true;
};

export const fetchColorsIfNeeded = () => (dispatch, getState) => {
  if (shouldFetchColors(getState())) {
    return dispatch(fetchColors());
  }
  return false;
};
