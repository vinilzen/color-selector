import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from '../actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates RECIEVE_COLORS when fetching colors has been done', () => {
    fetchMock
      .getOnce('http://www.mocky.io/v2/5a37a7403200000f10eb6a2d', ['123', '345']);

    const expectedActions = [
      {
        type: actions.REQUEST_COLORS,
        isFetching: true
      },
      {
        type: actions.RECIEVE_COLORS,
        colors: ['123', '345']
      }
    ];

    const store = mockStore({ colors: [] });

    return store.dispatch(actions.fetchColors()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
