import React from 'react';
import PropTypes from 'prop-types';

const SearchForm = ({ onChange }) => (
  <div>
    <form>
      <div className="form-group">
        <input
          className="form-control form-control-lg"
          id="colorInput"
          type="text"
          aria-describedby="colorHelp"
          onChange={e => onChange(e.target.value)}
          placeholder="Type color name or hex value"
        />
        <small id="colorHelp" className="form-text text-muted">
          Autosuggest will work from 2 chars
        </small>
      </div>
    </form>
  </div>
);

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default SearchForm;
