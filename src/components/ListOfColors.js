import React from 'react';
import PropTypes from 'prop-types';

export default function ListOfColors(props) {
  function isMatched(searchString, color) {
    if (searchString && searchString !== '' && searchString.length > 1) {
      const re = new RegExp(searchString, 'i');
      return re.test(color.name) || re.test(color.hex);
    }
    return true;
  }

  const {
    colors, searchString, pickColor, message, isFetching
  } = props;

  return (
    <ul className="text-left list-unstyled">
      {(colors.length === 0 && message !== '') && (
        <li
          className="text-center text-danger"
          role="alert"
        >
          {message}
        </li>
      )}
      {(colors.length === 0 && isFetching) && (
        <li>
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              aria-valuenow="100"
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: '100%' }}
            />
          </div>
        </li>)}
      {colors.map(color => (
        <li
          key={color.hex}
          className={`mb-1 ${isMatched(searchString, color) ? '' : 'd-none'}`}
        >
          <span style={{ color: `#${color.hex}` }} >◼︎</span>
          {`${color.name} #${color.hex} `}
          <button
            className="btn btn-sm btn-secondary float-right"
            onClick={() => { pickColor(color); }}
          >
            choose
          </button>
          <div className="clearfix" />
        </li>))}
    </ul>
  );
}

ListOfColors.propTypes = {
  colors: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    hex: PropTypes.string.isRequired
  })).isRequired,
  searchString: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  pickColor: PropTypes.func.isRequired
};
